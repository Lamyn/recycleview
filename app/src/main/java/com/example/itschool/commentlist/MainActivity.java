package com.example.itschool.commentlist;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static int position;
    RecyclerView recyclerView;
    SortingAdapter adapter;
    Spinner spinner;
    private String[] variants = {"Likes", "Dislike", "Views"};
    ArrayList<Sorting> list = new ArrayList<>();
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> spinneAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                variants);

        spinner.setAdapter(spinneAdapter);

        list.add(new Sorting("Кот", 25, 88, 100));
        list.add(new Sorting("Собака", 44, 78, 150));
        list.add(new Sorting("Лемур", 38, 89, 72));
        list.add(new Sorting("Суслик", 21, 16, 9));
        list.add(new Sorting("Барсук", 19, 72, 302));
        list.add(new Sorting("Енот", 97, 58, 111));
        list.add(new Sorting("Мышь", 15, 32, 122));



        spinner.setOnItemSelectedListener(this);




        adapter = new SortingAdapter(this, list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        position=pos;
        Collections.sort(list);
        adapter = new SortingAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
