package com.example.itschool.commentlist;

import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;

import static com.example.itschool.commentlist.MainActivity.position;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class Sorting implements Comparable{
    private String username;
    private String img;
    private int likes;
    private int views;
    private int dislikes;
    public Sorting(String username, int likes, int views, int dislikes) {
        this.username = username;
        this.likes=likes;
        this.views=views;
        this.dislikes = dislikes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    @Override
    public int compareTo(Object obj) {
        if (obj instanceof Sorting) {
            Sorting sor = (Sorting) obj;
            if (position == 0) {
                if (this.likes > sor.likes) {
                    return -1;
                } else if (this.likes < sor.likes) {
                    return 1;
                }

            }
            if (position == 2){
                if (this.views > sor.views) {
                    return -1;
                }else if(this.views < sor.views){
                    return 1;
                }
            }
            if (position == 1){
                if (this.dislikes > sor.dislikes) {
                    return -1;
                }else if(this.dislikes < sor.dislikes){
                    return 1;
                }
            }

        }
        return 0;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }
}
