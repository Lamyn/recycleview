package com.example.itschool.commentlist;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class SortingAdapter extends RecyclerView.Adapter<SortingAdapter.CommentViewHolder> {

    ArrayList<Sorting> sortings;
    Context context;

    SortingAdapter(Context context, ArrayList<Sorting> list) {
        this.context = context;
        this.sortings = list;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.d("MYLOG", "create view holder");
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_layout, null);
        CommentViewHolder commentView = new CommentViewHolder(itemLayoutView);
        return commentView;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(CommentViewHolder viewHolder, int i) {
        Log.d("MYLOG", "bind view holder");
        Sorting comment = sortings.get(i);
        viewHolder.mTVUsername.setText(comment.getUsername());
        viewHolder.mTVlikes.setText(String.valueOf(comment.getLikes()));
        viewHolder.mTVviews.setText(String.valueOf(comment.getViews()));
        viewHolder.mTVdate.setText(String.valueOf(comment.getDislikes()));
    }

    @Override
    public int getItemCount() {
        return sortings.size();
    }


    class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView mIVAvatar;
        TextView mTVUsername, mTVlikes, mTVviews, mTVdate;

        public CommentViewHolder(View rootView) {
            super(rootView);
            mIVAvatar = (ImageView) rootView.findViewById(R.id.iv_avatar);
            mTVUsername = (TextView) rootView.findViewById(R.id.tv_username);
            mTVlikes = (TextView) rootView.findViewById(R.id.tv_likes);
            mTVviews = (TextView) rootView.findViewById(R.id.tv_views);
            mTVdate = (TextView) rootView.findViewById(R.id.tv_date);
        }
    }


}
